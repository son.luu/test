import { Dimensions, Platform, StatusBar } from 'react-native'

export const Metrics = {
  WINDOW_WIDTH: Dimensions.get('window').width,
  WINDOW_HEIGHT: Dimensions.get('window').height,
  isIphoneX: () => {
    const d = Dimensions.get('window')
    const isX = !!(Platform.OS === 'ios' && (d.height > 800 || d.width > 800))
    return isX
  },
  statusBarHeight: () => {
    if (Platform.OS === 'android') {
      return StatusBar.currentHeight
    } else {
      const d = Dimensions.get('window')
      const isX = d.height > 800 || d.width > 800
      if (isX) {
        return 44
      } else {
        return 20
      }
    }
  },
  get: (pixels: number) => {
    const d = Dimensions.get('window')
    const designWidth = 750
    const deviceWidth = d.width * d.scale
    const percent = deviceWidth / designWidth
    if (percent < 1) {
      return Math.ceil(pixels * percent)
    }
    return pixels
  }
}


import {
    createInfo
  } from '../component/UserCard/UserCard.view'
  
  describe('UserCard', () => {
    describe('createInfo', () => {
      test('should render correctly createInfo', () => {
        const userInfo = {
            name: {
                first: 'Danny',
                last: 'Jame'
            },
            phone: '(637)-592-7223',
            password: '123456',
            location: {
                city: 'boulder',
                street: '3633 rolling green rd'
            }
        }

        const activeIndex = 1

        const result = {
          lable: 'My name is',
          value: 'Danny Jame'
        }

        expect(createInfo(userInfo, activeIndex)).toEqual(result)
      })
    })
  })
  
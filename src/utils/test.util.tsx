import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'

configure({adapter: new Adapter()})

const singleSnapTest = (wrapper: any, description: any) => {
  test(description, () => {
    expect(wrapper).toMatchSnapshot()
  })
}
const testSnapshots = (Component: any, configs: any) => {
  describe('snapshots', () => {
    configs.forEach((config: any) => {
      const { props, description } = config
      const wrapper = shallow(<Component {...props} />)
      singleSnapTest(wrapper, description)
    })
  })
}

export { singleSnapTest, testSnapshots }
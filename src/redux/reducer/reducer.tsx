import {combineReducers} from 'redux';
import {login} from './login.reducer';

export const reducers = (state: any, action: any) =>
  combineReducers({login})(state, action);

import {TYPES} from '../actions/actions.type';

const INIT_STATE = {
  token: '',
};

export const login = (state = INIT_STATE, action: any) => {
  switch (action.type) {
    case TYPES.SAVE_ACCESS_TOKEN:
      return {
        token: action.data,
      };
    default:
      return state;
  }
};

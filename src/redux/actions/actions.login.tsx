import {TYPES} from './actions.type';

export const saveAccessToken = (accessToken: String) => {
  return {
    type: TYPES.SAVE_ACCESS_TOKEN,
    data: accessToken,
  };
};

import { testSnapshots } from '../../utils/test.util'

import TabComponent from './TabComponent.component'

const props = {}

describe('TabComponent', () => {
  testSnapshots(TabComponent, [
    {
      props,
      description: 'render TabComponent'
    }
  ])
})

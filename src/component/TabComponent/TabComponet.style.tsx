import glamorous from 'glamorous-native'

const Components = {
  TouchableOpacity : glamorous.touchableOpacity({
    width: '50%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    // color: '#2c2e31'
  }),
  ContentText : glamorous.text({
    fontSize: 20,
    color: '#2c2e31',
    // color: '#fff'
  })
}

export default Components
import * as React from 'react'

import Components from './TabComponet.style'

const { TouchableOpacity, ContentText } = Components
const TabComponent: React.SFC<any> = ({ name, focused, onPress }) => (
    <TouchableOpacity
      activeOpacity={1}
      style={[focused ? { backgroundColor: '#2c2e31' } : {}]}
      onPress={() => onPress()}
    >
      <ContentText style={[focused ? { color: '#fff' } : {}]}>
        {name}
      </ContentText>
    </TouchableOpacity>
)

export default TabComponent

import { testSnapshots } from '../../utils/test.util'

import UserCard from './UserCard.view'

const props = {}

describe('UserCard', () => {
  testSnapshots(UserCard, [
    {
      props,
      description: 'render UserCard'
    }
  ])
})
import { isEmpty, pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import { SafeAreaView, Text, View, TouchableOpacity, Image } from 'react-native'
import { Metrics } from '../../common/Metrics'
import Components, { styles } from './UserCard.style'

const ItemWidth = Metrics.WINDOW_WIDTH * 0.9
const ItemHeight = ItemWidth * 1.1
const padding = (Metrics.WINDOW_WIDTH - ItemWidth) /2

interface IInnerProps {
  listUser: any
  setListUser(listUser: any): void
  activeIndex: any
  addFavorite(user: any): void
  favoriteList: any
  SET_INFO_ACTIVE(id: number): void
}

interface IOuterProps {
  deleteItem?(user: any): void 
  width?: any
  userInfo?: any
  inFavorite: boolean
}

type EnhancedProps = IInnerProps & IOuterProps

const { Container } = Components

const IconInfo = [
    {
      id: 1,
      iconActive: require('../../assets/icon/user_active.png'),
      icon: require('../../assets/icon/user.png'),
    },
    {
      id: 2,
      iconActive: require('../../assets/icon/address_active.png'),
      icon: require('../../assets/icon/address.png'),
    },
    {
      id: 3,
      iconActive: require('../../assets/icon/call_active.png'),
      icon: require('../../assets/icon/call.png'),
    },
    {
      id: 4,
      iconActive: require('../../assets/icon/lock_active.png'),
      icon: require('../../assets/icon/lock.png'),
    },
  ];

const createInfo = (userInfo: any, activeIndex: number) => {
  const firstName = pathOr('', ['name', 'first'], userInfo)
  const lastName = pathOr('', ['name', 'last'], userInfo)
  const street = pathOr('', ['location', 'street'], userInfo)
  const city = pathOr('', ['location', 'city'], userInfo)
  const phone = pathOr('', ['phone'], userInfo)
  const password = pathOr('', ['password'], userInfo)
  switch (activeIndex) {
    case 1:
      return {
        lable: 'My name is',
        value: `${firstName} ${lastName}`
      }
    case 2:
      return {
        lable: 'My address is',
        value: `${street} ${city}`
      }
    case 3:
      return {
        lable: 'My phone number is',
        value: `${phone}`
      }
    case 4:
      return {
        lable: 'My password is',
        value: `${password}`
      }
    default:
      return {
        lable: 'My name is',
        value: `${firstName} ${lastName}`
      };
  }
}

const renderInfoText = (userInfo: any, activeIndex: number) => {
  let info = createInfo(userInfo, activeIndex)
    return (
      <View style={styles.info}>
        <Text style={styles.lable}>{info.lable}</Text>
        <Text numberOfLines={1} style={styles.value}>
          {info.value}
        </Text>
      </View>
    )
}



const UserCardView: FunctionComponent<EnhancedProps> = (props) => {
  const { activeIndex, userInfo, width, inFavorite } = props
  const picture = pathOr('',['picture'], userInfo)
  const heightSlide = width
  const heightHeader = width * 0.4 - 20
  const widthAvatarContainer = width * 0.4
  const avatarRadius = widthAvatarContainer / 2
  const avatarSource = { uri: !isEmpty(picture) ? picture : 'http://api.randomuser.me/portraits/men/25.jpg' }

    return (
    <SafeAreaView>
        <View style={[styles.container, { width: width, height: heightSlide, marginRight: inFavorite ? padding : 0 }]}>
        <View style={[styles.header, { height: heightHeader }]}>
          {props.deleteItem && (
            <TouchableOpacity
              activeOpacity={1}
              style={styles.close}
              onPress={() => props.deleteItem ? props.deleteItem(userInfo) : null}>
              <Text style={styles.closeText}>
                X
              </Text>
            </TouchableOpacity>
          )}

          <View
            style={[
              styles.avatarContainer,
              {
                height: widthAvatarContainer,
                width: widthAvatarContainer,
                borderRadius: avatarRadius,
              },
            ]}>
            <Image
              source={avatarSource}
              style={[
                styles.avatar,
                { borderRadius: avatarRadius },
              ]}
            />
          </View>
        </View>
        {renderInfoText(userInfo, activeIndex)}
        <View style={styles.iconList}>
          {IconInfo.map(item => {
            let imageSrc =
              item.id === props.activeIndex ? item.iconActive : item.icon;
            return (
              <TouchableOpacity
                activeOpacity={1}
                key={item.id}
                onPress={() => props.SET_INFO_ACTIVE(item.id)}
                style={styles.typeInfoButton}>
                <Image source={imageSrc} style={styles.iconInfo} />
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
        </SafeAreaView>
    )
}
  

export { IInnerProps, IOuterProps, EnhancedProps, renderInfoText, createInfo}
export default UserCardView

import glamorous from 'glamorous-native'
import { StyleSheet } from 'react-native'

const Components = {
  Container : glamorous.view({
    alignItems: 'center',
    alignSelf: 'center'
  })
}

const styles = StyleSheet.create({
    container: {
      width: '100%',
      alignItems: 'center',
      backgroundColor: '#fff',
      display: 'flex',
      justifyContent: 'space-between',
      borderRadius: 10,
      shadowOffset: { width: 1, height: 1 },
      shadowColor: 'rgba(0,0,0,.25)',
      shadowOpacity: 0.4,
      elevation: 1
    },
    header: {
      width: '100%',
      height: '27%',
      borderBottomWidth: 1,
      backgroundColor: '#f9f9f9',
      borderColor: 'rgba(0,0,0,.25)',
      alignItems: 'center',
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
    },
    avatarContainer: {
      marginTop: 20,
      backgroundColor: '#fff',
      borderWidth: 1,
      borderColor: 'rgba(0,0,0,.25)',
      padding: 3,
      marginBottom: -70,
      zIndex: 99,
    },
    avatar: {
      height: '100%',
      width: '100%',
    },
    info: {
      paddingTop: 20,
    },
    lable: {
      textAlign: 'center',
      fontSize: 20,
      color: '#999',
    },
    value: {
      textAlign: 'center',
      fontSize: 30,
      color: '#2c2e31',
      paddingHorizontal: 5,
    },
    iconList: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginBottom: 15,
    },
    typeInfoButton: {
      marginHorizontal: 1,
    },
    iconInfo: {
      width: 41,
      height: 47,
    },
    close: {
      position: 'absolute',
      top: 0,
      right: 5,
      padding: 7
    },
    closeText: {
      fontSize: 18,
      color: 'rgba(0,0,0,.45)'
    }
  })

export { styles } 
export default Components

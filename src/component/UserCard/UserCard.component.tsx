import { compose, withState, withHandlers } from 'recompose'
import handlers from './UserCard.handler'
import { IInnerProps, IOuterProps } from './UserCard.view'
import UserCard from './UserCard.view'

const enhance = compose<IInnerProps,IOuterProps>(
  withState('activeIndex', 'setActiveIndex', 1),
  withHandlers(handlers)
)

export default enhance(UserCard)

import React, {SFC} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './Login.view.style';

interface IProps {
  saveAccessToken(accessToken: String): void;
  ON_LOGIN(): void;
}

const LoginView: SFC<IProps> = props => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={1}
        key={'login_key'}
        onPress={() => props.ON_LOGIN()}
        style={styles.buttonLogin}>
        <Text style={styles.textLogin}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

export {IProps};
export default LoginView;

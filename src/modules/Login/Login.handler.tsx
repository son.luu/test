import {IProps} from './Login.view';

export default {
  ON_LOGIN: (props: IProps & any) => async () => {
    try {
      const response = await Promise.resolve(
        (await fetch('https://sandbox.101digital.io/token', {
          method: 'post',
          headers: new Headers({
            Authorization:
              'Basic TGJUZVVFT2RpTEhhZzV4aUxpWDdPQ3ZFbmZNYTpiWVNtbFRBakxsVDJuWEc1SVh2QjNLRDdvVm9h',
            'Content-Type': 'application/x-www-form-urlencoded',
          }),
          body: 'grant_type=client_credentials&scope=PRODUCTION',
        })).json(),
      );

      props.saveAccessToken(response.access_token);

      props.navigation.navigate('Home');
    } catch (error) {
      console.log('error', error);
    }
  },
};

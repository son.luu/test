import {compose, withHandlers} from 'recompose';
import handlers from './Login.handler';
import LoginView from './Login.view';
import {IProps} from './Login.view';
import {connect} from 'react-redux';
import {saveAccessToken} from '../../redux/actions/actions.login';

const mapDispatchToProps = {
  saveAccessToken: saveAccessToken,
};

const enhance = compose<IProps, IProps>(
  connect(
    null,
    mapDispatchToProps,
  ),
  withHandlers(handlers),
);

export default enhance(LoginView);

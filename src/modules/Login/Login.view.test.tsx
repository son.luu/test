import {testSnapshots} from '../../utils/test.util';

import FavoriteView from './Login.view';

const props = {};

describe('FavoriteView', () => {
  testSnapshots(FavoriteView, [
    {
      props: {
        listFavoriteUser: [],
      },
      description: 'render FavoriteView',
    },
  ]);
});

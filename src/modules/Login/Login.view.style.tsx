import {StyleSheet} from 'react-native';
import {Metrics} from '../../common/Metrics';

const ItemWidth = Metrics.WINDOW_WIDTH * 0.9;
const ItemHeight = ItemWidth * 1.1;
const PaddingCarousel = (Metrics.WINDOW_WIDTH - ItemWidth) / 2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  carosel: {
    height: 200,
  },
  caroselContainer: {
    height: ItemHeight,
    zIndex: 9,
    backgroundColor: '#f9f9f9',
  },
  containerCustomStyle: {
    paddingHorizontal: PaddingCarousel,
  },
  temp1: {
    width: '100%',
    height: Metrics.WINDOW_HEIGHT * 0.2,
    backgroundColor: '#2c2e31',
  },
  temp2: {
    width: '100%',
    height: ItemWidth * 0.2,
    position: 'absolute',
    zIndex: 0,
    marginRight: -10,
    backgroundColor: '#2c2e31',
  },
  buttonLogin: {
    width: 100,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
    padding: 10,
  },
  textLogin: {
    color: 'white',
  },
});

export default styles;

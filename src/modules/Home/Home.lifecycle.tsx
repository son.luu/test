import {IProps} from './Home.view';

const lifecycle = {
  async onDidMount(props: IProps) {
    try {
      const response = await Promise.resolve(
        (await fetch(
          'https://sandbox.101digital.io/invoice-service/1.0.0/invoices?fromDate=2019-06-10&toDate=2020-06-10&pageSize=10&pageNum=1&merchantReference=3011047',
          {
            method: 'get',
            headers: new Headers({
              Authorization: `Bearer ${props.accessToken}`,
            }),
          },
        )).json(),
      );
      props.setListInvoice(response.data);
    } catch (error) {
      console.log('error', error);
    }
  },
};

export default lifecycle;

import withLifecycle from '@hocs/with-lifecycle';
import {compose, setStatic, withState, withHandlers} from 'recompose';
import handlers from './Home.handler';
import HomeView from './Home.view';
import {IProps} from './Home.view';
import {connect} from 'react-redux';
import lifecycle from './Home.lifecycle';

const mapStateToProps = (state: any) => ({
  accessToken: state.login.token,
});

const enhance = compose<IProps, IProps>(
  connect(
    mapStateToProps,
    null,
  ),
  setStatic('navigationOptions', {
    header: null,
  }),
  withState('listInvoice', 'setListInvoice', []),
  withHandlers(handlers),
  withLifecycle(lifecycle),
);

export default enhance(HomeView);

import React, {SFC} from 'react';
import {View, FlatList, Text} from 'react-native';
import {styles} from './Home.view.style';

interface IProps {
  accessToken: string;
  listInvoice: [];
  setListInvoice(listInvoice: []): void;
}

const renderItem = (item: any) => {
  return <Text style={styles.title}>{item.item.invoiceId}</Text>;
};

const HomeView: SFC<IProps> = props => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
      <FlatList
        data={props.listInvoice}
        renderItem={renderItem}
        keyExtractor={(item: any) => item.invoiceId}
      />
    </View>
  );
};

export {IProps};
export default HomeView;

import glamorous from 'glamorous-native';
import {StyleSheet} from 'react-native';

const Components = {
  Container: glamorous.view({
    alignItems: 'center',
    alignSelf: 'center',
  }),
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default Components;
export {styles};

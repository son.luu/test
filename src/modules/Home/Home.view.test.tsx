import { testSnapshots } from '../../utils/test.util'

import HomeView from './Home.view'

const props = {}

describe('HomeView', () => {
  testSnapshots(HomeView, [
    {
      props: {
        listUser: []
      },
      description: 'render HomeView'
    }
  ])
})

import ApolloClient from 'apollo-boost';
import {ThemeProvider} from 'glamorous-native';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {Provider} from 'react-redux';

import Navigator from './src/modules/Navigation';

import {store, persistor} from './src/redux/configStore';

import {PersistGate} from 'redux-persist/integration/react';

const client = new ApolloClient();
const theme = {
  colors: {},
  fonts: {},
  size: {
    label: 12,
    normal: 14,
    medium: 16,
    title: 18,
  },
};

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <PersistGate loading={null} persistor={persistor}>
        <ApolloProvider client={client}>
          <Navigator />
        </ApolloProvider>
      </PersistGate>
    </ThemeProvider>
  </Provider>
);

export default App;
